import random

randomNumber = random.randint(1,9)
guess = 0
track = 0

while guess != randomNumber and guess != "exit":
    guess = input("Unesite broj izmedu 1 i 9: ")

    if guess == "exit":
        break
    guess = int(guess)

    track +=1

    if  guess > randomNumber:
        print("Unijeli ste broj koji je veci od random broja")
    elif guess < randomNumber:
        print("Unijeli ste broj koji je manji od random broja")
    else:
        print("Unijeli ste tocan broj!")
        print("Broj pokusaja: " + str(track))
    
