


#Najbolji Git tutorijal

git 
test

git status - Naredba kojom provjeravamo da li se što mijenjalo u postojećem repozitoriju. Pokretanjem naredbe git status ispisati će se datoteke koje se ne prate ili datoteke koje su dodane ukoliko smo upisali git add

git add - Naredba kojom dodajemo izmjenjeni ili novi sadržaj za commit. primjer naredbe je git add git-tutor.md

git log - Naredba kojom provjeravamo kada su se dogodile promjene u repozitoriju, odnosno commit-ovi. Pokretanjem naredbe imamo uvid u broj commit-a, tko je bio autor, e-mail adresu autora commit-a, kada se commit dogodio, te poruku koju je autor ostavio prilikom commit-a.

git commit - Naredba kojom nakon dodavanja određene promjene ili nove datoteke sa naredbom add potvrđujemo da smo spremni postaviti promjenu na push

git diff - Naredba kojom dobijamo uvid u promjene koje su se dogodile od zadnjeg commit-a nakon spremanja. Ukoliko pokrenemo naredbu git diff vidimo točno na kojoj liniji se dogodila promjena, što se izmijenilo, te tko je autor promjene.

git config - Naredba kojom mijenjamo podatke o korisniku koji radi promjene. primjer naredbe: git config user.name "vjeran.bozic". Također možemo mijenjati i e-mail korisnika.

git pull - Naredba koja povlači sve promjene sa udaljenog repozitorija na naš lokalni repozitorij.
         - Koristi se na način: git pull master origin gdje origin predstavlja naziv udaljenog repozitorija, a master predstavlja naziv brancha.
