  316 mkdir lab5                                                        #kreiramo lab5 direktoriji

  317 cd lab5                                                           #ulazimo u lab5 oz se_labs

  318  ls                                                               #ispisujemo što se nalazi u lab5

  319  python -m venv django                                            #kreiramo virual environment po imenu django

  320  source django/Scripts/activate                                   #aktiviramo virutal environment

  321  pip install django                                               #instaliramo django
  
  322  django-admin startproject myimgur                                #kreiramo novi projekt po imenu "myimgur"

  323  ls                                                               #ispisujemo sadržaj lab5 da vidimo je li projekt kreiran

  324  pip freeze > requirements.txt                                    #upisuje potrebne stvari za projekt

  325  cat requirements.txt                                             #ispisuje ih sve
  
  326  cd ..                                                            #vracamo se u se_labs

  327  git status                                                       #provjeravamo status, ima li promjena

  328  git add lab5                                                     #dodajemo lab5 da ga prati

  329  git status                                                       #provjeravamo prati li lab5

  330  git add .gitignore                                               #dodajemo gitignoru pracenju

  331  git commit -m "Add lab5 starter project and .gitignore"          #commitamo sve što smo dodali s odgovarajucom porukom
